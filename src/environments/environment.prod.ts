export const environment = {
    production: true,
    baseURL: 'https://gs-back.atapi.fr/api',
    health_baseURL: 'https://gs-back-atapi.fr/health-check',
};
