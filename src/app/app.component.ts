import {Component} from '@angular/core';
import { Router, NavigationStart, NavigationEnd} from '@angular/router';
import { HealthCheckService } from './services/health-check.service';
import { tap } from 'rxjs/operators';




@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Atapi';

    constructor(
        private router: Router,
        private healthCheck: HealthCheckService
    ){
  
    }
    ngOnInit() {
        this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationStart) {

                this.healthCheck.healthCheck().pipe(
                    tap(
                      (data) => {
                      },
                      (error) => {
                      }
                    ),
                    //retryWhen(errors => errors.pipe(delay(5000)))
                  ).subscribe();

                // Show loading indicator
                /*this.accountService.getUserById('3')
                .subscribe(
                    data => {
                        
                      },
                      error => {
                        this.accountService.logout();
                      });*/
          
            }
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0)
        });
    }
  
}
