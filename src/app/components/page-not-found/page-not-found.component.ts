import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  t: Observable<object>;

  constructor(private route: ActivatedRoute) {}

  public some_data : String;
  

  ngOnInit() {
    this.route
      .data
      .subscribe(v => console.log(v['some_data']));
  }



}
