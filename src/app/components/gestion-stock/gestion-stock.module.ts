import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GestionStockRoutingModule } from './gestion-stock-routing.module';
import { CategoryComponent } from './category/category.component';
import { ProductComponent } from './product/product.component';
import { BaseLayoutComponent } from '../layoutModels/base-layout/base-layout.component';
import { MatTableModule } from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms';

import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  declarations: [
    BaseLayoutComponent,
    CategoryComponent,
    ProductComponent,
  ],
  imports: [
    CommonModule,
    GestionStockRoutingModule,
    MatTableModule,
    ReactiveFormsModule,
    MatInputModule,
    MatAutocompleteModule
  ],
})
export class GestionStockModule { }
