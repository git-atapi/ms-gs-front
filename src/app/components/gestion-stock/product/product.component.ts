import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AlertService } from 'src/app/services/alert.service';
import { GSService } from 'src/app/services/gs.service';


export interface ProductElement {
  idCategory: string,
  nomProduit: string,
  qte: string,
  qteConsom: string,
  prix: string,
  datePeremption: string,
  dateArrivee: string
}


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(
    private gsService: GSService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) { }

  errorMessage: any;

  addProduitForm: FormGroup;
  searchProductForm: FormGroup;

  produitSubmitted = false;
  runningSearch = false;


  categoryList: any;


  // auto Complete
  myControl = new FormControl();
  options: string[];
  filteredOptions: Observable<string[]>;

  displayedColumns: string[] = ['idCategory', 'nomProduit', 'qte', 'qteConsom', 'prix', 'datePeremption', 'dateArrivee'];
  dataSource: ProductElement[];


  ngOnInit(): void {
    this.initForms();
    this.getAllCategories();
    this.getAllProducts();

  }

  get addProduitFormControl() { return this.addProduitForm.controls; }

  get searchProduitFormControl() { return this.searchProductForm.controls; }

  initForms() {
    this.addProduitForm = this.formBuilder.group({
      idCategory: ['', Validators.required],
      nomProduit: ['', Validators.required],
      qte: ['', Validators.required],
      qteConsom: ['', Validators.required],
      prix: ['', Validators.required],
      datePeremption: ['', Validators.required],
      dateArrivee: ['', Validators.required]
    });

    this.searchProductForm = this.formBuilder.group({
      idCategory: ['', Validators.required],
    });


  }

  onAddProduitSubmit() {
    this.produitSubmitted = true;

    // reset alert on submit
    this.alertService.clear;
    this.addProduitForm.controls['idCategory'].setValue(this.curCat);


    // stop if register form is not valid
    if (this.addProduitForm.invalid) {
      this.alertService.errorToast("Probléme lors de l'ajout d'un produit", "Produit");
      return;
    }

    this.gsService.addProduct(this.addProduitForm.value)
      .subscribe(
        data => {
          this.alertService.alertToast("produit ajouté avec succès", "Produit");
          this.getAllProducts();
        },
        error => {
          console.log(error);
          this.alertService.errorToast("Probléme lors de l'ajout d'un produit", "Produit");

        });
  }

  getAllProducts() {

    this.gsService.getAllProducts().subscribe(
      (data) => {
        // Try to run this code
        if (data) {
          this.dataSource = data;
        } else {
          this.dataSource = [];
        }

      },
      (error) => {
        // if any error, Code throws the error
        this.errorMessage = error.error.message;
        this.alertService.errorToast("Probléme lors de recherche de category", "Produit");
      }
    );
  }

  onSearchProductSubmit() {

    this.runningSearch = true;

    // reset alert on submit
    this.alertService.clear;

    const idCat = this.getCategoryNameByName(this.searchProductForm.controls['idCategory'].value).id;


    // stop if register form is not valid
    if (this.searchProductForm.invalid) {
      this.alertService.errorToast("Probléme lors de la recherche des produits", "Produit");
      this.runningSearch = false;
      return;
    }

    this.gsService.getAllAvailableProducts(idCat).subscribe(
      (data) => {
        // Try to run this code
        if (data) {
          this.dataSource = data;
        } else {
          this.dataSource = [];
        }

      },
      (error) => {
        // if any error, Code throws the error
        this.errorMessage = error.error.message;
        this.alertService.errorToast("Probléme lors de recherche de category", "Produit");
      }
    );
  }


  getAllCategories() {

    this.gsService.getAllCategories().subscribe(
      (data) => {
        // Try to run this code
        if (data) {
          this.categoryList = data;
          this.options = data.map(t => t.nomCategory);
          this.filteredOptions = this.myControl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value)),
          );

        }

      },
      (error) => {
        // if any error, Code throws the error
        this.errorMessage = error.error.message;
        this.alertService.errorToast("Probléme lors de recherche de category", "Produit");
      }
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  curCat: any;;
  setNewCateg(cat: any): void {
    this.curCat = cat;
  }

  getCategoryNameByKey(key: string) {
    return this.categoryList.find(x => x.id === key);
  }

  getCategoryNameByName(nomCategory: string) {
    return this.categoryList.find(x => x.nomCategory === nomCategory);
  }




}
