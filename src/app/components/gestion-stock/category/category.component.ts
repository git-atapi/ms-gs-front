import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { GSService } from 'src/app/services/gs.service';


export interface CategoryElement {
  id: string;
  nomCategory: string;
}

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(
    private gsService: GSService,
    private formBuilder: FormBuilder,
    private alertService: AlertService

  ) { }

  displayedColumns: string[] = ['id', 'nomCategory'];
  dataSource: CategoryElement[];

  addCategoryForm: FormGroup;

  CategorySubmitted = false;


  ngOnInit(): void {
    this.getAllCategories();
    this.initForms();
  }

  get addCategoryFormControl() { return this.addCategoryForm.controls; }


  initForms() {
    this.addCategoryForm = this.formBuilder.group({
      nomCategory: ['', Validators.required]
    });
  }

  onAddCategorySubmit() {
    this.CategorySubmitted = true;


    // stop if register form is not valid
    if (this.addCategoryForm.invalid) {
      this.alertService.errorToast("Probléme lors de l'ajout d'une nouvelle catégorie", "Catégorie");
      return;
    }

    this.gsService.addCategory(this.addCategoryForm.value)
      .subscribe(
        data => {
          this.getAllCategories();
          this.alertService.alertToast("catégorie ajoutée avec succès", "Catégorie");
        },
        error => {
          console.log(error);
          this.alertService.errorToast("Probléme lors de l'ajout d'une nouvelle catégorie", "Catégorie");

        });
  }




  getAllCategories() {
    this.gsService.getAllCategories().subscribe(
      (data) => {
        // Try to run this code
        if (data) {
          this.dataSource = data;
        }

      },
      (error) => {
        // if any error, Code throws the error
        console.log(error.error.message, 'error');
      }
    );
  }

}
