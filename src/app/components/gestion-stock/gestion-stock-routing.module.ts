import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseLayoutComponent } from '../layoutModels/base-layout/base-layout.component';
import { CategoryComponent } from './category/category.component';
import { ProductComponent } from './product/product.component';

const routes: Routes = [
  {
    path: '', component: BaseLayoutComponent,
    children: [
        { path: 'category', component: CategoryComponent },
        { path: 'product', component: ProductComponent}
    ]
  },
  { path: '**', redirectTo: '/404' }

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionStockRoutingModule { }
