import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Version} from './version';

@Injectable({
    providedIn: 'root'
})
export class VersionService {
    private baseUrl = 'http://localhost:9991/api/versions';

    constructor(private http: HttpClient) {
    }

    getVersion(): Observable<Version[]> {
        return this.http.get<Version[]>(`${this.baseUrl}`);
    }

}
