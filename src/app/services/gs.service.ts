import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GSService {
  private gsURL = environment.baseURL;

  constructor(private http: HttpClient) { }


  getAllCategories() {
    const url = `${this.gsURL}/gs/get-all-categories`;
    return this.http.get(url).pipe(catchError(this.errorHandler));
  }

  addCategory(category: any) {
    return this.http.post(`${this.gsURL}/gs/add-category`, category)
  }

  getAllProducts() {
    const url = `${this.gsURL}/gs/get-all-products`;
    return this.http.get(url).pipe(catchError(this.errorHandler));
  }

  addProduct(product: any) {
    return this.http.post(`${this.gsURL}/gs/add-product`, product)
  }

  getAllAvailableProducts(idCategory: any) {
    const url = `${this.gsURL}/gs/get-all-availableProducts?idCategory=` + idCategory;
    return this.http.get(url).pipe(catchError(this.errorHandler));
  }



  /*
  
  addPost(post: any) {
    return this.http.post(`${this.slideUrl}/add-new-slide`, post)
  }
  
  postComment(comment: any){
    return this.http.post(`${this.blogUrl}/comments`, comment)
  }*/


  errorHandler(error: HttpErrorResponse) {
    return new Observable((observer: Observer<any>) => {
      observer.error(error);
    });
  }
}