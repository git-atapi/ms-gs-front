import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private subject = new Subject<Alert>();
  private defaultId = 'default-alert';

  constructor(private toastr: ToastrService) { }

  // enable subscribing to alerts observable
  onAlert(id = this.defaultId): Observable<Alert> {
    return this.subject.asObservable().pipe(
      filter(
        x => x && x.id == id
      )
    );
  }

  toastParms = {
    positionClass: 'toast-bottom-right',
    closeButton: true,
    progressBar: true
  }


  alertToast(message: string, title: string) {
    this.toastr.success(message, title, this.toastParms);
  }

  wanringoast(message: string, title: string) {
    this.toastr.warning(message, title, this.toastParms);
  }

  errorToast(message: string, title: string) {
    this.toastr.error(message, title, this.toastParms);
  }

  // main alert method    
  alert(alert: Alert) {
    alert.id = alert.id || this.defaultId;
    this.subject.next(alert);
  }

  // clear alerts
  clear(id = this.defaultId) {
    this.subject.next(new Alert({ id }));
  }

  success(message: string, options?: any) {
    this.alert(new Alert({
      ...options,
      type: AlertType.Success, message
    }));
  }

  error(message: string, options?: any) {
    this.alert(new Alert({
      ...options,
      type: AlertType.Error,
      message
    }));
  }

  info(message: string, options?: any) {
    this.alert(new Alert({
      ...options,
      type: AlertType.Info,
      message
    }));
  }

  warn(message: string, options?: any) {
    this.alert(new Alert({
      ...options,
      type: AlertType.Warning,
      message
    }));
  }





}

export class Alert {
  id: string;
  type: AlertType;
  message: string;
  autoClose: boolean;
  keepAfterRouteChange: boolean;
  fade: boolean;

  constructor(init?: Partial<Alert>) {
    Object.assign(this, init);
  }
}

export enum AlertType {
  Success,
  Error,
  Info,
  Warning
}