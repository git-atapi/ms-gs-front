import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HealthCheckService {

  constructor(
    private http: HttpClient) { }

  healthCheck() {
    return this.http.get<any>(`${environment.health_baseURL}/health`, { observe: 'response' }).pipe(
      map((resp: any) => {
        if(resp.status !== 200){
          throw resp;
        }
        return resp.status;
      }),
      //repeatWhen(() => interval(5000))
    );
  }
}
