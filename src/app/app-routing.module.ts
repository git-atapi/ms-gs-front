import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/content-base/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const gestionStockModule = () => import('./components/gestion-stock/gestion-stock.module').then(x => x.GestionStockModule);


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'index', component: HomeComponent},

  { path: 'gs', loadChildren: gestionStockModule},

  { path: '**', redirectTo: '/404' },
  { path: '404', component: PageNotFoundComponent,  data : {some_data : "only-404"} }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
 })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
