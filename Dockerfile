FROM node:14-slim
MAINTAINER khaled

RUN mkdir -p /app
WORKDIR /app

COPY . .

RUN npm install
RUN npm install -g @angular/cli
#RUN npm install localstorage-polyfill


#EXPOSE 6005

#RUN npm run build:ssr
#RUN npm run serve:ssr
#CMD ["node", "dist/portfolio-front/server/main.js"]


